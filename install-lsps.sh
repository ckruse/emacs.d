#!/bin/bash

npm -g --prefix ~/.config/emacs/lsp/ts-ls install typescript-language-server
npm -g --prefix ~/.config/emacs/lsp/vscode-langservers-extracted install vscode-langservers-extracted
npm -g --prefix ~/.config/emacs/lsp/graphql-language-service-cli/ install graphql-language-service graphql-language-service-cli
npm -g --prefix ~/.config/emacs/lsp/eslint_d install eslint_d

mkdir -p ~/.config/emacs/lsp/elixir-ls/
rm ~/.config/emacs/lsp/elixir-ls/*
curl https://api.github.com/repos/elixir-lsp/elixir-ls/releases/latest | jq -r '.assets[0] | .browser_download_url' | xargs -I {} wget -O ~/.config/emacs/lsp/elixir-ls/latest.zip {}
pushd ~/.config/emacs/lsp/elixir-ls/
unzip latest.zip
rm latest.zip
popd

uname -s | grep -i darwin > /dev/null
IS_MACOS=$?

uname -s | grep -i linux > /dev/null
IS_LINUX=$?

RUST_ANALYZER_FILE=""

if [[ $IS_MACOS = 0 ]]; then
  RUST_ANALYZER_FILE="rust-analyzer-x86_64-apple-darwin"
elif [[ $IS_LINUX = 0 ]]; then
  RUST_ANALYZER_FILE="rust-analyzer-x86_64-unknown-linux-gnu"
else
  echo "Unsupported OS"
  exit 1
fi

mkdir -p ~/.config/emacs/lsp/rust/
curl -L https://github.com/rust-lang/rust-analyzer/releases/latest/download/${RUST_ANALYZER_FILE}.gz | gunzip -c - > ~/.config/emacs/lsp/rust/rust-analyzer
chmod 755 ~/.config/emacs/lsp/rust/rust-analyzer
