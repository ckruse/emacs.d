(load (expand-file-name "bootstrap.el" user-emacs-directory))

(require 'ck-org)

;; helpers
(require 'ck-defuns)

;; basics and appearance
(require 'ck-basics)
(require 'ck-appearance)

;; enhancements
(require 'ck-vertico)
(require 'ck-packages)
(require 'ck-dired)

(require 'ck-lsp)
(require 'ck-web)
(require 'ck-elixir)
(require 'ck-rust)
(require 'ck-git)
(require 'ck-autocomplete)
(require 'ck-treemacs)

(require 'ck-treesitter)

(use-package emacs
  :ensure nil
  :init
  ;; Add prompt indicator to `completing-read-multiple'.
  ;; Alternatively try `consult-completing-read-multiple'.
  (defun crm-indicator (args)
    (cons (concat "[CRM] " (car args)) (cdr args)))
  (advice-add #'completing-read-multiple :filter-args #'crm-indicator)

  ;; Do not allow the cursor in the minibuffer prompt
  (setq minibuffer-prompt-properties
        '(read-only t cursor-intangible t face minibuffer-prompt))
  (add-hook 'minibuffer-setup-hook #'cursor-intangible-mode)

  ;; Emacs 28: Hide commands in M-x which do not work in the current mode.
  ;; Vertico commands are hidden in normal buffers.
  (setq
    read-extended-command-predicate #'command-completion-default-include-p
    ;; Enable recursive minibuffers
    enable-recursive-minibuffers t))

;; last setup keybindings
(require 'ck-keybindings)
