(defun ck/kill-all-buffers ()
  (interactive)
  (mapcar 'kill-buffer (buffer-list))
  (delete-other-windows))

(defun ck/comment-line-or-region (n)
  "Comment or uncomment current line and leave point after it.
  With positive prefix, apply to N lines including current one.
  With negative prefix, apply to -N lines above.
  If region is active, apply to active region instead."
  (interactive "p")
  (if (use-region-p)
      (comment-or-uncomment-region
       (region-beginning) (region-end))
    (let ((range
           (list (line-beginning-position)
                 (goto-char (line-end-position n)))))
      (comment-or-uncomment-region
       (apply #'min range)
       (apply #'max range)))
    (forward-line 1)
    (back-to-indentation)))

(defun ck/light-theme ()
  (interactive)
  (setq highlight-indent-guides-auto-odd-face-perc 2
        highlight-indent-guides-auto-even-face-perc 4)
  (load-theme 'doom-one-light t))

(defun ck/dark-theme ()
  (interactive)
  (setq highlight-indent-guides-auto-odd-face-perc 15
        highlight-indent-guides-auto-even-face-perc 7.5)
  (load-theme 'doom-one t))

(defun ck/ripgrep ()
  (interactive)
  (consult-ripgrep
   (if (projectile-project-p)
       (projectile-project-root)
     nil)))

;; stolen from Doom emacs
(defun +javascript-add-npm-path-h ()
  "Add node_modules/.bin to `exec-path'."
  (when-let* ((node-modules-parent (locate-dominating-file default-directory "node_modules/"))
              (node-modules-dir (expand-file-name "node_modules/.bin/" node-modules-parent))
              (sys-path (getenv "PATH")))
    (make-local-variable 'exec-path)
    (add-to-list 'exec-path node-modules-dir)

    (when (not (string-match-p node-modules-dir sys-path))
      (setenv "PATH" (concat sys-path ":" node-modules-dir)))

    (message ":lang:javascript: add %s to $PATH" (expand-file-name "node_modules/" node-modules-parent))))

(defun ck/add-lsp-path (path)
  "Add lsp path to `exec-path'."
  (let ((path (expand-file-name path user-emacs-directory))
        (sys-path (getenv "PATH")))
    (make-local-variable 'exec-path)
    (add-to-list 'exec-path path)

    (when (not (string-match-p path sys-path))
      (setenv "PATH" (concat (getenv "PATH") ":" path)))

    (message ":lsp: add %s to $PATH" path)))

(defun ck/increment-number-at-point ()
  "Increment a number at point."
  (interactive)
  (let* ((bounds (bounds-of-thing-at-point 'word))
         (beg (car bounds))
         (end (cdr bounds))
         (num (string-to-number (buffer-substring beg end)))
         (value (+ num 1)))
    (delete-region beg end)
    (insert (format "%d" value))))

(defun ck/decrement-number-at-point ()
  "Decrement a number at point."
  (interactive)
  (let* ((bounds (bounds-of-thing-at-point 'word))
         (beg (car bounds))
         (end (cdr bounds))
         (num (string-to-number (buffer-substring beg end)))
         (value (- num 1)))
    (delete-region beg end)
    (insert (format "%d" value))))


(defun ck/copy-file-name-to-clipboard ()
  (interactive)
  (let ((filename (if (equal major-mode 'dired-mode)
                      default-directory
                    (buffer-file-name))))
    (when filename
      (kill-new filename)
      (message "Copied buffer file name '%s' to the clipboard." filename))))

(defun ck/copy-file-name-project-root-to-clipboard ()
  (interactive)
  (let ((filename (if (equal major-mode 'dired-mode)
                      default-directory
                    (buffer-file-name)))
        (root (if (projectile-project-p)
                  (projectile-project-root)
                "")))
    (when filename
      (kill-new (replace-regexp-in-string root "" filename))
      (message "Copied buffer file name '%s' to the clipboard." (replace-regexp-in-string root "" filename)))))



(provide 'ck-defuns)
