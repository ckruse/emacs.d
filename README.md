# CK's personal emacs config

This is my personal Emacs config. It mainly focuses on programming in various environments, including but not limited to:

- Web development (HTML, JS, CSS, TS, React)
- Elixir
- Rust

Additionally I use `org-mode` for todo and task tracking. 
