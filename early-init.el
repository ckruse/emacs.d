(setq package-enable-at-startup nil
      orig-gc-cons-threshold gc-cons-threshold
      ;; we set GC limits very high during startup to avoid GCing during
      gc-cons-threshold (* 1024 1024 1024)
      load-prefer-newer t)

(defvar native-comp-deferred-compilation-deny-list nil)

(setenv "LSP_USE_PLISTS" "true")

(when (or (featurep 'ns)
          (featurep 'mac))
  (add-to-list 'default-frame-alist '(ns-transparent-titlebar . t))
  (add-to-list 'default-frame-alist '(ns-appearance . dark)))

(when (string-equal system-type "gnu/linux")
  (add-to-list 'default-frame-alist '(undecorated . t)))

(add-to-list 'default-frame-alist '(alpha-background . 95))
