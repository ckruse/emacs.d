(use-package general
  :config
  (defun ck/expand-region ()
    (interactive)
    (if (bound-and-true-p combobulate-mode)
        (combobulate-mark-node-dwim)
      (er/expand-region 1)))

  (general-define-key :keymaps 'projectile-mode-map
                      "C-c p s" 'consult-ripgrep
                      "C-c p S" 'deadgrep)

  (general-define-key :keymaps 'minibuffer-mode-map
                      "C-x C-s" 'embark-export)

  (general-define-key "M-g g" 'consult-goto-line

                      "C-c q F" 'ck/kill-all-buffers

                      "s-<up>" 'beginning-of-buffer
                      "s-<down>" 'end-of-buffer

                      "s-u" 'revert-buffer
                      "C-x o" 'ace-window
                      ;; "C-x C-b" 'bufler
                      "C-x C-b" 'ibuffer

                      "s-z" 'undo-fu-only-undo
                      "s-Z" 'undo-fu-only-redo

                      "s-s" 'save-buffer
                      "s-c" 'kill-ring-save
                      "s-x" 'kill-region
                      "s-v" 'yank

                      "C-c c C-c" 'ck/comment-line-or-region
                      "s-/" 'ck/comment-line-or-region
                      "s-a" 'mark-whole-buffer

                      "s-d" 'mc/mark-next-like-this
                      "s-=" 'ck/expand-region
                      "s-+" 'ck/expand-region
                      "s--" 'er/contract-region

                      "M-<up>" 'drag-stuff-up
                      "M-<down>" 'drag-stuff-down
                      "M-<left>" 'drag-left-stuff
                      "M-<right>" 'drag-stuff-right
                      "C-c v g" 'magit-status
                      "s-g" 'git-timemachine

                      "C-x C-u" 'upcase-dwim
                      "C-x C-l" 'downcase-dwim

                      "C-c c t" 'treemacs-select-window)

  (general-create-definer ck/leader-def
    :prefix "C-c c")

  (ck/leader-def "c"
    (defhydra consult-hydra (:color blue)
      "consult actions"
      ("g" consult-outline "outline")
      ("l" consult-line "search line")
      ("r" ck/ripgrep "ripgrep")))

  (ck/leader-def "f"
    (defhydra often-used-files (:color blue)
      "often used files"
      ("p" (find-file "~/org/passwords.org.gpg") "open passwords file")
      ("i" (find-file "~/org/inbox.org") "Open org-mode inbox file")
      ("t" (find-file "~/org/work/termitel.org") "Open termitel org-mode file")))

  (ck/leader-def "e"
    (defhydra expand-region (:color red)
      "expand region as a hydra"
      ("e" er/expand-region "Expand region")
      ("s" er/contract-region "Shrink region")))

  (ck/leader-def "n"
    (defhydra numbers (:color red)
      "increment / decrement number"
      ("<up>" ck/increment-number-at-point "increment")
      ("<down>" ck/decrement-number-at-point "decrement")))

  (ck/leader-def "p"
    (defhydra project-actions-hydra (:color blue)
      "project actions"
      ("c" ck/copy-file-name-project-root-to-clipboard "Copy filename to clipboard")
      ("f" ck/copy-file-name-to-clipboard "Copy filename to clipboard")))

  (ck/leader-def "m"
    (defhydra hydra-multiple-cursors (:hint nil)
      "
 Up^^             Down^^           Miscellaneous           % 2(mc/num-cursors) cursor%s(if (> (mc/num-cursors) 1) \"s\" \"\")
------------------------------------------------------------------
 [_p_]   Next     [_n_]   Next     [_l_] Edit lines  [_0_] Insert numbers
 [_P_]   Skip     [_N_]   Skip     [_a_] Mark all    [_A_] Insert letters
 [_M-p_] Unmark   [_M-n_] Unmark   [_s_] Search      [_q_] Quit
 [_|_] Align with input CHAR       [Click] Cursor at point"
      ("l" mc/edit-lines :exit t)
      ("a" mc/mark-all-like-this :exit t)
      ("n" mc/mark-next-like-this)
      ("N" mc/skip-to-next-like-this)
      ("M-n" mc/unmark-next-like-this)
      ("p" mc/mark-previous-like-this)
      ("P" mc/skip-to-previous-like-this)
      ("M-p" mc/unmark-previous-like-this)
      ("|" mc/vertical-align)
      ("s" mc/mark-all-in-region-regexp :exit t)
      ("0" mc/insert-numbers :exit t)
      ("A" mc/insert-letters :exit t)
      ("<mouse-1>" mc/add-cursor-on-click)
      ;; Help with click recognition in this hydra
      ("<down-mouse-1>" ignore)
      ("<drag-mouse-1>" ignore)
      ("q" nil)))
    ;; (defhydra multicursor (:color red)
    ;;   "multicursor"
    ;;   ("v" mc/edit-lines "all lines")
    ;;   ("d" mc/mark-next-like-this "next match")
    ;;   ("p" mc/mark-previous-like-this "prev match")
    ;;   ("D" mc/mark-all-like-this "all matches")
    ;;   ("h" mc-hide-unmatched-lines-mode "hide unmatched lines")))

  (ck/leader-def "o"
    (defhydra org (:color blue)
      "org actions"
      ("a" org-agenda "Agenda")
      ("l" org-store-link "Store link")
      ("b" org-iswitchb "Org switch buffer")
      ("A" org-archive-subtree "Archive subtree" :color red)

      ("c" org-capture "Capture")
      ("i" org-clock-in "Punch in")
      ("o" org-clock-out "Punch out")
      ("g" org-clock-goto "Goto clock")

                                        ;("p" org-publish-all "Publish")
      ("t" ck/org-tags "list tags"))))

(when IS-MAC
  (setq mac-command-modifier      'super
        ns-command-modifier       'super
        mac-option-modifier       'meta
        ns-option-modifier        'meta
        ;; Free up the right option for character composition
        mac-right-option-modifier 'none
        ns-right-option-modifier  'none))

(unbind-key "C-x u")
(unbind-key "C-x g")

(provide 'ck-keybindings)
