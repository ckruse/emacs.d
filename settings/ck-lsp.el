;; (use-package eglot
;;   :ensure nil
;;   :bind (:map eglot-mode-map
;;               ("s-l r r" . eglot-rename)
;;               ("s-l a a" . eglot-code-actions)
;;               ;; goto definition is bound to M-.
;;               ("s-l g r" . xref-find-references)
;;               ("s-l g d" . eglot-find-declaration)
;;               ("s-l g t" . eglot-find-typeDefinition)
;;               ("s-l g i" . eglot-find-implementation))
;;   :config
;;   (setq-default eglot-workspace-configuration
;;                 '(:rust-analyzer (:check (:command "clippy"
;;                                           :features "all"
;;                                           :extraArgs ["--target-dir=target/analyzer"])
;;                                   :checkOnSave (:command "clippy"
;;                                           :features "all"
;;                                           :extraArgs ["--target-dir=target/analyzer"])
;;                                   :rustfmt (:extraArgs ["+nightly"])
;;                                   :procMacro (:enable t)
;;                                   :inlayHints (:chainingHints (:enable t)
;;                                                :closureReturnTypeHints (:enable t))))))

(use-package lsp-mode
  ;;:hook (lsp-mode . lsp-enable-which-key-integration)
  :hook (lsp-managed-mode . (lambda ()
                              (when (member major-mode '(js-ts-mode typescript-ts-mode tsx-ts-mode))
                                (flycheck-add-next-checker 'lsp 'javascript-eslint 'append))))
  :commands lsp lsp-deferred

  :preface
  (defun lsp-booster--advice-json-parse (old-fn &rest args)
    "Try to parse bytecode instead of json."
    (or
     (when (equal (following-char) ?#)

       (let ((bytecode (read (current-buffer))))
         (when (byte-code-function-p bytecode)
           (funcall bytecode))))
     (apply old-fn args)))
  (defun lsp-booster--advice-final-command (old-fn cmd &optional test?)
    "Prepend emacs-lsp-booster command to lsp CMD."
    (let ((orig-result (funcall old-fn cmd test?)))
      (if (and (not test?)                             ;; for check lsp-server-present?
               (not (file-remote-p default-directory)) ;; see lsp-resolve-final-command, it would add extra shell wrapper
               lsp-use-plists
               (not (functionp 'json-rpc-connection))  ;; native json-rpc
               (executable-find "emacs-lsp-booster"))
          (progn
            (message "Using emacs-lsp-booster for %s!" orig-result)
            (cons "emacs-lsp-booster" orig-result))
        orig-result)))

  :config
  (push "[/\\\\]_build\\'" lsp-file-watch-ignored-directories)
  (push "[/\\\\]deps\\'" lsp-file-watch-ignored-directories)
  (push "[/\\\\]cover\\'" lsp-file-watch-ignored-directories)
  (push "[/\\\\]priv\\'" lsp-file-watch-ignored-directories)
  (push "[/\\\\]target\\'" lsp-file-watch-ignored-directories)
  (push "[/\\\\]\\.deliver\\'" lsp-file-watch-ignored-directories)

  :init
  (setq lsp-use-plists t
        lsp-completion-provider :none
        lsp-enable-folding nil
        lsp-enable-text-document-color nil
        lsp-enable-on-type-formatting nil
        lsp-typescript-format-enable nil
        lsp-eslint-fomat nil
        lsp-javascript-format-enable nil
        lsp-headerline-breadcrumb-enable nil
        lsp-rust-analyzer-cargo-watch-command "clippy"
        lsp-rust-analyzer-cargo-watch-args ["--target-dir" "target/analyzer"]
        lsp-rust-analyzer-rustfmt-extra-args ["+nightly"]
        lsp-response-timeout 5
        lsp-rust-analyzer-experimental-proc-attr-macros t
        lsp-rust-analyzer-proc-macro-enable t
        lsp-inlay-hint-enable t)

  (advice-add (if (progn (require 'json)
                         (fboundp 'json-parse-buffer))
                  'json-parse-buffer
                'json-read)
              :around
              #'lsp-booster--advice-json-parse)
  (advice-add 'lsp-resolve-final-command :around #'lsp-booster--advice-final-command))

(use-package lsp-ui
  :commands lsp-ui-mode
  :init
  (setq lsp-ui-doc-enable nil
        lsp-ui-doc-show-with-cursor nil
        lsp-ui-doc-show-with-mouse nil
        lsp-ui-sideline-enable nil
        lsp-ui-sideline-show-code-actions nil
        lsp-ui-sideline-enable nil
        lsp-ui-sideline-show-hover nil
        lsp-ui-sideline-show-diagnostics nil))

(use-package lsp-treemacs
  :commands lsp-treemacs-errors-list)

;; (use-package eglot-booster
;;   :ensure (eglot-booster :type git :host github :repo "jdtsmith/eglot-booster")
;;   :after eglot
;;   :commands eglot-booster-mode
;;   :hook (on-first-input . eglot-booster-mode))

(provide 'ck-lsp)
