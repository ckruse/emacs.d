(use-package transient)

(use-package magit
  :commands magit-status
  :custom
  (magit-save-repository-buffers nil)
  :config
  (push (cons [unpushed status] 'show) magit-section-initial-visibility-alist)
  (push (cons [stashes status] 'show) magit-section-initial-visibility-alist))

(use-package git-timemachine
  :commands git-timemachine)

(use-package diff-hl
  :hook (find-file    . diff-hl-mode)
  :hook (vc-dir-mode  . diff-hl-dir-mode)
  :hook (dired-mode   . diff-hl-dired-mode)
  :hook (diff-hl-mode . diff-hl-flydiff-mode)
  :hook (magit-pre-refresh . diff-hl-magit-pre-refresh)
  :hook (magit-post-refresh . diff-hl-magit-post-refresh)

  :config
  (setq vc-git-diff-switches '("--histogram")
        diff-hl-flydiff-delay 0.5
        diff-hl-show-staged-changes nil))

(use-package gist
  :commands gist-region gist-region-private gist-buffer gist-buffer-private gist-region-or-buffer gist-region-or-buffer-private)

(provide 'ck-git)
