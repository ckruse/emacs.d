(use-package vertico
  :ensure (vertico :files (:defaults "extensions/*"))
  :hook (on-first-input . vertico-mode)
  :hook (rfn-eshadow-update-overlay . vertico-directory-tidy)
  :bind (:map vertico-map
              ([backspace] . vertico-directory-delete-char))
  :custom
  (vertico-count 20))

(use-package fussy
  :defer 0
  :config
  (push 'fussy completion-styles)
  (setq completion-category-defaults nil
        completion-category-overrides nil))


(use-package marginalia
  :hook (on-first-input . marginalia-mode)
  ;; Either bind `marginalia-cycle` globally or only in the minibuffer
  :bind (:map minibuffer-local-map
              ("M-A" . marginalia-cycle))

  :custom
  (marginalia-annotators '(marginalia-annotators-heavy marginalia-annotators-light nil))
  :config
  (setq marginalia-command-categories
        (append '((projectile-find-file . project-file)
                  (projectile-find-dir . project-file)
                  (projectile-switch-project . file))
                marginalia-command-categories)))

(use-package nerd-icons-completion
  :after (marginalia nerd-icons)
  :hook (marginalia-mode . nerd-icons-completion-marginalia-setup)
  :hook (on-first-input . nerd-icons-completion-mode))

(use-package embark
  :commands embark-prefix-help-command
  :init
  (setq prefix-help-command #'embark-prefix-help-command)
  :config

  ;; Hide the mode line of the Embark live/completions buffers
  (add-to-list 'display-buffer-alist
               '("\\`\\*Embark Collect \\(Live\\|Completions\\)\\*"
                 nil
                 (window-parameters (mode-line-format . none)))))

(use-package embark-consult
  :after (embark consult)
  :hook (embark-collect-mode . consult-preview-at-point-mode))

(use-package consult
  :bind (("M-y" . consult-yank-from-kill-ring)
         ("C-x b" . consult-buffer)))

(provide 'ck-vertico)
