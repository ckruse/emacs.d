(use-package rust-mode
  :ensure t
  :after treesit-auto
  :custom
  (rust-mode-treesitter-derive t))


(use-package rustic
  :ensure (rustic :type git :host github :repo "emacs-rustic/rustic")
  :hook (rustic-mode . ck/setup-rust-formatting)
  :after rust-mode
  :init
  (defun ck/setup-rust-formatting()
    (add-hook 'before-save-hook 'lsp-format-buffer nil t))

  (setq auto-mode-alist (delete '("\\.rs\\'" . rust-mode) auto-mode-alist))
  (setq auto-mode-alist (delete '("\\.rs\\'" . rust-ts-mode) auto-mode-alist)))

;; (defun ck/lsp-rust ()
;;   (ck/add-lsp-path ".cache/lsp/rust/")
;;   ;; (eglot-ensure))
;;   (lsp-deferred))

;; (use-package rust-ts-mode
;;   :ensure nil
;;   :after treesit-auto
;; ;;  :hook (rust-ts-mode . apheleia-mode)
;;   :mode ("\\.rs\\'" . rust-ts-mode)
;;   :hook (rust-ts-mode . ck/lsp-rust)
;;   :hook (rust-ts-mode . ck/setup-rust-formatting)
;;   :init
;;   (defun ck/setup-rust-formatting()
;;     (add-hook 'before-save-hook 'lsp-format-buffer nil t)))

(provide 'ck-rust)
