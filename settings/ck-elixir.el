(defun ck/lsp-elixir ()
  ;; (ck/add-lsp-path "lsp/elixir-ls/")
  ;; (eglot-ensure))
  (lsp-deferred))

(use-package heex-ts-mode
  :ensure (heex-ts-mode :type git :host github :repo "wkirschbaum/heex-ts-mode")
  :after treesit
  :hook (heex-ts-mode . ck/lsp-elixir)
  :mode "\\.[hl]eex\\'"
  :init
  (setq auto-mode-alist (delete '("\\.[hl]?eex\\'" . heex-ts-mode) auto-mode-alist))
  :config
  (setq auto-mode-alist (delete '("\\.[hl]?eex\\'" . heex-ts-mode) auto-mode-alist)))

(use-package elixir-ts-mode
  :ensure (elixir-ts-mode :type git :host github :repo "wkirschbaum/elixir-ts-mode")
  :after (treesit heex-ts-mode)
  :hook (elixir-ts-mode . ck/lsp-elixir)
  :hook (elixir-ts-mode . ck/setup-elixir-formatting)
  ;; :config
  ;; (add-to-list 'eglot-server-programs
  ;;              '(elixir-ts-mode "language_server.sh"))
  :init
  (defun ck/setup-elixir-formatting ()
  ;;   (add-hook 'before-save-hook 'eglot-format-buffer nil t)))
    (add-hook 'before-save-hook 'lsp-format-buffer nil t)))

(use-package erlang
  :commands erlang-mode
  ;; :hook (erlang-mode . eglot-ensure))
  :hook (erlang-mode . lsp-deferred))


(provide 'ck-elixir)
