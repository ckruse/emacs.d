(when (fboundp 'menu-bar-mode) (menu-bar-mode -1))
(when (fboundp 'tool-bar-mode) (tool-bar-mode -1))
(when (fboundp 'scroll-bar-mode) (scroll-bar-mode -1))

(setq maschine-name (replace-regexp-in-string "\\..*" "" (system-name)))
(setq ck/font (cond
               ((string= maschine-name "pug")
                "Hack Nerd Font-12")
               ((string= maschine-name "motte")
                "Hack Nerd Font Mono-12")
               (t
                "Hack Nerd Font-13")))

(let ((machine-file (expand-file-name "machine.el" user-emacs-directory)))
  (load machine-file 'noerror 'nomessage))

(add-to-list 'default-frame-alist `(font . ,ck/font))
(set-face-attribute 'default t :font ck/font)

(defadvice load-theme (before theme-dont-propagate activate)
  "Disable theme before loading new one."
  (mapc #'disable-theme custom-enabled-themes))

(use-package doom-themes
  :custom
  (doom-themes-enable-bold t)
  (doom-themes-enable-italic t)
  :init
  (load-theme 'doom-material-dark t)

  :config
  (doom-themes-visual-bell-config)
  (doom-themes-org-config))

(use-package doom-modeline
  :hook (elpaca-after-init . doom-modeline-mode)
  :hook (doom-modeline-mode . size-indication-mode)
  :hook (doom-modeline-mode . column-number-mode)
  :custom
  (doom-modeline-major-mode-color-icon t)
  (doom-modeline-github t))

(use-package nerd-icons
  :defer t)

(use-package nyan-mode
  :hook (on-first-input . nyan-mode))

(provide 'ck-appearance)
