(setq backup-inhibited t
      auto-save-default nil
      auto-save-list-file-prefix nil
      create-lockfiles nil
      make-backup-files nil

      inhibit-startup-message t
      initial-scratch-message nil

      display-time-24hr-format t
      display-time-day-and-date t
      display-time-default-load-average nil

      browse-url-browser-function 'browse-url-generic

      ;; performance tweaks
      idle-update-delay 1.0
      redisplay-skip-fontification-on-input t
      pgtk-wait-for-event-timeout 0.001

      user-full-name "Christian Kruse"
      user-mail-address "christian@kruse.cool"

      use-short-answers t
      confirm-kill-emacs 'y-or-n-p
      native-comp-async-report-warnings-errors nil

      kill-do-not-save-duplicates t
      mouse-yank-at-point t

      x-select-enable-clipboard t
      save-interprogram-paste-before-kill t
      ring-bell-function 'ignore
      visible-bell t
      line-move-visual nil
      track-eol t
      scroll-conservatively 1000
      delete-by-moving-to-trash t
      truncate-string-ellipsis "…")

(setq-default indent-tabs-mode nil
              tab-width 2
              fill-column 120
              show-trailing-whitespace t
              require-final-newline t
              truncate-lines t)

(use-package display-fill-column-indicator
  :ensure nil
  :hook (on-first-input . global-display-fill-column-indicator-mode)
  :custom
  (display-fill-column-indicator-column 80))

(setq browse-url-generic-program
      (if IS-MAC
          "open"
        "xdg-open"))

(when IS-MAC
  (setq insert-directory-program "gls"))

(use-package exec-path-from-shell
  :config
  (dolist (var '("SSH_AUTH_SOCK" "SSH_AGENT_PID" "GPG_AGENT_INFO" "LANG" "LC_CTYPE" "LSP_USE_PLISTS"))
    (add-to-list 'exec-path-from-shell-variables var))
  (exec-path-from-shell-initialize))

(use-package saveplace
  :ensure nil
  :hook (on-first-input . save-place-mode))

(use-package repeat-mode
  :ensure nil
  :hook (on-first-input . repeat-mode))


(display-time)

(when (fboundp 'global-font-lock-mode)
  (setq font-lock-maximum-decoration t)
  (global-font-lock-mode t))

(prefer-coding-system 'utf-8)
(set-default-coding-systems 'utf-8)
(setq locale-coding-system 'utf-8-unix)
(when (fboundp 'set-charset-priority)
  (set-charset-priority 'unicode))

(global-set-key (kbd "RET") 'newline-and-indent)

(delete-selection-mode t)
(global-hl-line-mode t)
(blink-cursor-mode 0)

(use-package which-function
  :ensure nil
  :hook (prog-mode . which-function-mode))

(when (boundp 'ns-pop-up-frames)
  (setq ns-pop-up-frames nil))

(use-package server
  :ensure nil
  :when (display-graphic-p)
  :hook (elpaca-after-init . (lambda ()
                               (unless (server-running-p)
                                 (server-start)))))

;; disable displaying bidirectional text
(setq-default bidi-display-reordering 'left-to-right
              bidi-paragraph-direction 'left-to-right)
(setq bidi-inhibit-bpa t
      inhibit-compacting-font-caches t)

(when (boundp 'ns-list-colors)
  (setq x-colors (ns-list-colors)))

(provide 'ck-basics)
