(use-package corfu
  :ensure (:files (:defaults "extensions/*") :includes (corfu-popupinfo))
  :hook (on-first-input . global-corfu-mode)

  :bind
  (:map corfu-map
        ("C-n" . corfu-next)
        ("C-p" . corfu-previous))

  :custom
  (corfu-auto t)
  (corfu-auto-delay 0.1)
  (corfu-auto-prefix 2)
  ;; (corfu-quit-at-boundary t)
  (corfu-quit-no-match t)
  (corfu-quit-at-boundary 'separator)
  (corfu-preselect-first nil)
  (corfu-max-width 120)

  :config
  (define-key corfu-map [up] nil)
  (define-key corfu-map [down] nil)
  (define-key corfu-map [remap previous-line] nil)
  (define-key corfu-map [remap next-line] nil))

(use-package cape
  :commands (cape-dabbrev
             cape-file
             cape-history
             cape-keyword
             cape-tex
             cape-sgml
             cape-rfc1345
             cape-abbrev
             cape-ispell
             cape-dict
             cape-symbol
             cape-line)
  :hook   ((html-mode web-mode tsx-ts-mode markdown-mode) . (lambda ()
                                                              (make-local-variable 'completion-at-point-functions)
                                                              (add-to-list 'completion-at-point-functions #'cape-sgml t)))
  ;; :hook (lsp-mode . (lambda ()
  ;;                     (make-local-variable 'completion-at-point-functions)
  ;;                     (add-to-list 'completion-at-point-functions #'lsp-completion-at-point)))
  ;; :hook (eglot-managed-mode . (lambda ()
  ;;                               (make-local-variable 'completion-at-point-functions)
  ;;                               (add-to-list 'completion-at-point-functions #'eglot-completion-at-point)))
  :hook (lsp-mode . (lambda ()
                      (advice-add #'lsp-completion-at-point :around #'cape-wrap-noninterruptible)))
  :init
  (add-to-list 'completion-at-point-functions #'cape-file)
  (add-to-list 'completion-at-point-functions #'cape-abbrev)
  (add-to-list 'completion-at-point-functions #'cape-dabbrev)
  (add-to-list 'completion-at-point-functions #'cape-keyword)

  (add-to-list 'completion-styles 'partial-completion t)
  (add-to-list 'completion-styles 'initials t))

  ;; (defalias 'cape:lsp-cape (cape-capf-super
  ;;                           #'lsp-completion-at-point
  ;;                           #'cape-abbrev
  ;;                           #'cape-dabbrev
  ;;                           #'cape-keyword))

  ;; (add-hook 'lsp-mode-hook
  ;;           (defun ck/add-lsp-capf ()
  ;;               (setq-local completion-at-point-functions '(cape:lsp-cape cape-file)))))

  ;; (add-hook 'eglot-managed-mode-hook
  ;;           (defun ck/add-lsp-capf ()
  ;;             (if (eglot-managed-p)
  ;;               (setq-local completion-at-point-functions '(cape:lsp-cape cape-file))
  ;;               (kill-local-variable 'completion-at-point-functions)))))

(use-package nerd-icons-corfu
  :ensure (nerd-icons-corfu :type git :host github :repo "LuigiPiucco/nerd-icons-corfu")
  :commands nerd-icons-corfu-formatter
  :hook (corfu-mode . (lambda () (add-to-list 'corfu-margin-formatters #'nerd-icons-corfu-formatter))))

(use-package dabbrev
  :ensure nil
  :config
  (setq dabbrev-ignored-buffer-regexps '("\\.\\(?:pdf\\|jpe?g\\|png\\)\\'")))

(setq read-extended-command-predicate
      #'command-completion-default-include-p)

(use-package corfu-history
  :ensure nil
  :after savehist
  :hook (corfu-mode . corfu-history-mode)
  :config
  (add-to-list 'savehist-additional-variables 'corfu-history))

(use-package corfu-popupinfo
  :ensure nil
  :after corfu
  :hook (corfu-mode . corfu-popupinfo-mode)
  :config
  (setq corfu-popupinfo-delay '(0.5 . 1.0)))

(provide 'ck-autocomplete)
