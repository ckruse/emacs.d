(use-package pfuture
  :defer t)

(use-package treemacs
  :defer t
  :init
  (treemacs-project-follow-mode t))

(use-package treemacs-projectile
  :after (treemacs projectile)
  :ensure nil
  :defer t)


(use-package treemacs-magit
  :after (treemacs magit)
  :ensure nil
  :defer t)

(use-package treemacs-nerd-icons
  :hook (treemacs-mode . (lambda () (treemacs-load-theme "nerd-icons"))))

;; (use-package lsp-treemacs
;;   :after (treemacs lsp-mode treemacs-all-the-icons)
;;   :defer t)

(provide 'ck-treemacs)
