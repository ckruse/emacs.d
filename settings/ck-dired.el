(use-package dired
  :ensure nil
  :bind
  (:map dired-mode-map
        ("RET" . dired-find-alternate-file)
        ("a" . dired-find-file))
  :custom
  (dired-dwim-target t)
  (dired-recursive-copies 'always)
  (dired-isearch-filenames 'dwim)
  (dired-listing-switches "-alh")
  (dired-auto-revert-buffer #'dired-buffer-stale-p)
  (dired-create-destination-dirs 'ask)
  (dired-vc-rename-file t)
  (dired-ls-F-marks-symlinks IS-MAC)
  (dired-hide-details-hide-symlink-targets nil)
  :config
  (put 'dired-find-alternate-file 'disabled nil))

(use-package emacs-async
  :ensure (emacs-async :type git :host github :repo "jwiegley/emacs-async" :main "async.el")
  :commands dired-async-mode
  :hook (on-first-input . dired-async-mode))

(use-package diredfl
  :hook (dired-mode . diredfl-mode)
  :config
  (set-face-attribute 'diredfl-dir-name nil :bold t))

(use-package dired-x
  :ensure nil
  :defer 0
  :hook (dired-mode . dired-omit-mode)
  :custom
  (dired-omit-verbose nil)
  (dired-clean-confirm-killing-deleted-buffers nil)
  :config
  (setq dired-omit-files
        (concat dired-omit-files
                "\\|^\\.DS_Store\\'"
                "\\|^\\.project\\(?:ile\\)?\\'"
                "\\|^\\.\\(?:svn\\|git\\)\\'"
                "\\|^\\.ccls-cache\\'"
                "\\|\\(?:\\.js\\)?\\.meta\\'"
                "\\|\\.\\(?:elc\\|o\\|pyo\\|swp\\|class\\)\\'"))

  (let ((cmd (cond
              (IS-MAC "open")
              (t "xdg-open"))))
    (setq dired-guess-shell-alist-user
          `(("\\.pdf\\'" ,cmd)
            ("\\.docx\\'" ,cmd)
            ("\\.\\(?:djvu\\|eps\\)\\'" ,cmd)
            ("\\.\\(?:jpg\\|jpeg\\|png\\|gif\\|xpm\\)\\'" ,cmd)
            ("\\.\\(?:xcf\\)\\'" ,cmd)
            ("\\.csv\\'" ,cmd)
            ("\\.tex\\'" ,cmd)
            ("\\.\\(?:mp4\\|mkv\\|avi\\|flv\\|rm\\|rmvb\\|ogv\\)\\(?:\\.part\\)?\\'" ,cmd)
            ("\\.\\(?:mp3\\|flac\\)\\'" ,cmd)
            ("\\.html?\\'" ,cmd)
            ("\\.md\\'" ,cmd)))))

(use-package nerd-icons-dired
  :hook (dired-mode . nerd-icons-dired-mode))

(use-package fd-dired
  :defer t
  :init
  (global-set-key [remap find-dired] #'fd-dired))

(provide 'ck-dired)
