(use-package org
  :commands org-mode org-agenda org-capture
  :hook (org-mode . turn-on-font-lock)
  :mode ("\\.\\(org\\|org_archive\\)$" . org-mode)
  :init
  (setq org-directory "~/org"
        org-agenda-files (list org-directory)
        org-default-notes-file (concat org-directory "/inbox.org")

        org-log-into-drawer t

        org-agenda-start-with-follow-mode t

        org-todo-keywords '((sequence "TODO(t)" "NEXT(n)" "|" "DONE(d)")
                            (sequence "WAITING(w@/!)" "HOLD(h@/!)" "|" "CANCELLED(c@/!)" "PHONE" "MEETING"))

        org-use-fast-todo-selection t
        org-treat-S-cursor-todo-selection-as-state-change nil
        org-todo-state-tags-triggers '(("CANCELLED" ("CANCELLED" . t))
                                       ("WAITING" ("WAITING" . t))
                                       ("HOLD" ("WAITING") ("HOLD" . t))
                                       (done ("WAITING") ("HOLD"))
                                       ("TODO" ("WAITING") ("CANCELLED") ("HOLD"))
                                       ("NEXT" ("WAITING") ("CANCELLED") ("HOLD"))
                                       ("DONE" ("WAITING") ("CANCELLED") ("HOLD")))

        org-capture-templates '(("t" "todo" entry (file+headline "~/org/inbox.org" "Inbox")
                                 "* TODO %?\n%U\n%a\n" :clock-in t :clock-resume t)
                                ("n" "note" entry (file+headline "~/org/notes.org" "Notizen")
                                 "* %?\n%U\n" :clock-in t :clock-resume t)
                                ("w" "blog entry" entry (file+headline "~/org/inbox.org" "Inbox")
                                 "* TODO %? :blog:\n%U\n" :clock-in t :clock-resume t)
                                ("j" "Journal" entry (file+datetree "~/org/priv/diary.org")
                                 "* %?\n%U\n" :clock-in t :clock-resume t)
                                ("m" "Meeting" entry (file+headline "~/org/inbox.org" "Inbox")
                                 "* MEETING with %? :MEETING:\n%U" :clock-in t :clock-resume t)
                                ("c" "Phone call" entry (file+headline "~/org/inbox.org" "Inbox")
                                 "* PHONE %? :PHONE:\n%U" :clock-in t :clock-resume t)
                                ("p" "password" entry (file "~/org/passwords.gpg")
                                 "* %^{Title}\n  %^{URL}p %^{USERNAME}p %^{PASSWORD}p"))

        org-refile-targets '((nil :maxlevel . 2)
                             (org-agenda-files :maxlevel . 2))
        org-refile-use-outline-path 'file
        org-outline-path-complete-in-steps nil
        org-refile-allow-creating-parent-nodes 'confirm

        org-agenda-custom-commands '(("f" "FLOSS" tags-todo ":private:foss:"
                                      ((org-agenda-overriding-header "FLOSS")))
                                     ("p" "Priv-TODOs" tags-todo ":private:todo:"
                                      ((org-agenda-overriding-header "Priv-TODOs")))
                                     ("w" "Work todos" tags-todo "+work+termitel-someday-ettlingen"
                                      ((org-agenda-overriding-header "Work todos")))
                                     ("s" "Work todos: someday" tags-todo "+work+termitel+someday-ettlingen"
                                      ((org-agenda-overriding-header "Work todos: someday")))
                                     ("e" "Work todos: Ettlingen" tags-todo "+work+termitel+ettlingen"
                                      ((org-agenda-overriding-header "Work todos: Ettlingen"))))
        ))

(use-package org-bullets
  :hook (org-mode . org-bullets-mode)
  :after org)

(provide 'ck-org)
