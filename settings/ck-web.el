(defun ck/vscode-langservers-lsp ()
  ;; (ck/add-lsp-path "lsp/vscode-langservers-extracted/bin")
  ;; (eglot-ensure))
  (lsp-deferred))

;; (use-package flymake-eslint
;;   :commands flymake-eslint-enable
;;   :custom
;;   (flymake-eslint-defer-binary-check t)
;;   :hook (eglot-managed-mode . (lambda ()
;;                                 (when (derived-mode-p 'typescript-ts-mode 'js-ts-mode 'tsx-ts-mode)
;;                                   (flymake-eslint-enable)))))

(use-package web-mode
  :hook (web-mode . lsp-deferred)
  :hook (web-mode . +javascript-add-npm-path-h)
  :mode (("\\.html\\'" . web-mode)
         ("\\.html\\.eex\\'" . web-mode)
         ("\\.html\\.heex\\'" . web-mode)
         ("\\.html\\.tera\\'" . web-mode)
         ("\\.html\\.jinja\\'" . web-mode)
         ("\\.html\\.j2\\'" . web-mode))
  :custom
  (web-mode-markup-indent-offset 2)
  (web-mode-css-indent-offset 2)
  (web-mode-code-indent-offset 2)
  (web-mode-enable-auto-indentation nil)
  (web-mode-enable-auto-pairing nil)
  (web-mode-engines-alist '(("django" . "\\.html\\.tera\\'")))
  :config
  (with-eval-after-load 'lsp-mode
    (add-to-list 'lsp-language-id-configuration '("\\.html\\.j2$" . "html"))
    (add-to-list 'lsp-language-id-configuration '("\\.html\\.jinja$" . "html"))))

(use-package css-ts-mode
  :ensure nil
  :mode ("\\.css\\'" . css-ts-mode)
  :hook (css-ts-mode . +javascript-add-npm-path-h)
  :hook (css-ts-mode . ck/vscode-langservers-lsp)
  :hook (css-ts-mode . apheleia-mode)
  :custom (css-indent-offset 2))

(use-package scss-mode
  :ensure nil
  :mode ("\\.scss\\'" . scss-mode)
  :hook (scss-mode . +javascript-add-npm-path-h)
  :hook (scss-mode . apheleia-mode)
  :hook (scss-mode . ck/vscode-langservers-lsp)
  :custom (css-indent-offset 2))


(use-package json-ts-mode
  :ensure nil
  :mode (("\\.json\\'" . json-ts-mode))
  :hook (json-ts-mode . +javascript-add-npm-path-h)
  :hook (json-ts-mode . apheleia-mode)
  :hook (json-ts-mode . ck/vscode-langservers-lsp))

(defun ck/ts-lsp ()
  ;; (ck/add-lsp-path "lsp/ts-ls/bin")
  ;; (eglot-ensure))
  (lsp-deferred))

(use-package js-ts-mode
  :ensure nil
  :mode (("\\.js\\'" . js-ts-mode)
         ("\\.jsx\\'" . js-ts-mode))
  :hook (js-ts-mode . +javascript-add-npm-path-h)
  :hook (js-ts-mode . apheleia-mode)
  :hook (tsx-ts-mode . ck/ts-lsp)
  :custom (js-indent-level 2))

(use-package typescript-ts-mode
  :ensure nil
  :mode (("\\.ts\\'" . typescript-ts-mode))
  :hook (typescript-ts-mode . +javascript-add-npm-path-h)
  :hook (typescript-ts-mode . apheleia-mode)
  :hook (typescript-ts-mode . ck/ts-lsp)
  :custom (js-indent-level 2))

(use-package tsx-ts-mode
  :ensure nil
  :mode (("\\.tsx\\'" . tsx-ts-mode))
  :hook (tsx-ts-mode . +javascript-add-npm-path-h)
  :hook (tsx-ts-mode . apheleia-mode)
  :hook (tsx-ts-mode . ck/ts-lsp)
  ;:hook (tsx-ts-mode . tsx-ts-helper-mode)
  :custom (js-indent-level 2))

(use-package astro-ts-mode
  :mode (("\\.astro\\'" . astro-ts-mode))
  :hook (astro-ts-mode . +javascript-add-npm-path-h)
  :hook (astro-ts-mode . apheleia-mode)
  :hook (astro-ts-mode . lsp-deferred)
  :config
  (add-to-list 'treesit-language-source-alist
               '(astro "https://github.com/virchau13/tree-sitter-astro"))
  (add-to-list 'treesit-language-source-alist
               '(css "https://github.com/tree-sitter/tree-sitter-css")))

(provide 'ck-web)
