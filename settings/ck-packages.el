(use-package diminish
  :defer 0)

(use-package subword
  :ensure nil
  :hook (on-first-input . global-subword-mode))

(use-package glyphless-display-mode
  :ensure nil
  :hook (on-first-input . glyphless-display-mode))

(use-package show-paren
  :ensure nil
  :hook (on-first-input . show-paren-mode)
  :custom
  (show-paren-context-when-offscreen t))

(use-package isearch
  :ensure nil
  :custom
  (isearch-lazy-count t))

(use-package display-line-numbers
  :ensure nil
  :commands display-line-numbers
  :hook (prog-mode . display-line-numbers-mode))

(use-package which-key
  :diminish which-key-mode
  :hook (on-first-input . which-key-mode)
  :custom
  (which-key-separator " ")
  (which-key-prefix-prefix "+"))

(use-package puni
  :hook (on-first-input . puni-global-mode)
  :custom
  (puni-confirm-when-delete-unbalanced-active-region nil)
  :bind (:map puni-mode-map
              ("C-h" . puni-force-delete)))

(use-package electric
  :ensure nil
  :hook ((prog-mode text-mode) . electric-pair-local-mode))

(use-package uniquify
  :ensure nil
  :defer 0
  :config
  (setq uniquify-buffer-name-style 'post-forward uniquify-separator ":"))

(use-package mwim
  :bind (([home] . mwim-beginning-of-code-or-line)
         ([s-left] . mwim-beginning-of-code-or-line)
         ([end] . mwim-end-of-code-or-line)
         ([s-right] . mwim-end-of-code-or-line)))

(use-package yasnippet
  :hook (on-first-input . yas-global-mode)
  :diminish yas-minor-mode)

(use-package expand-region
  :commands expand-region)

(use-package multiple-cursors
  :commands
  mc/edit-lines mc/mark-all-like-this mc/mark-next-like-this
  mc/skip-to-next-like-this mc/unmark-next-like-this mc/mark-previous-like-this
  mc/skip-to-previous-like-this mc/unmark-previous-like-this mc/vertical-align
  mc/mark-all-in-region-regexp mc/insert-numbers mc/insert-letters
  mc/add-cursor-on-click mc/num-cursors)

(use-package ace-window
  :commands ace-window
  :custom-face
  (aw-leading-char-face ((t (:foreground "red" :height 3.0)))))

(use-package hydra
  :commands defhydra)

(use-package undo-fu
  :commands (undo-fu-only-undo undo-fu-only-redo))

(use-package drag-stuff
  :commands drag-stuff-up drag-stuff-down drag-stuff-left drag-stuff-right)

(use-package projectile
  :hook (on-first-input . projectile-mode)
  :diminish projectile-mode
  :custom
  (projectile-keymap-prefix (kbd "C-c p"))
  (projectile-require-project-root nil))

(use-package deadgrep
  :commands deadgrep
  :custom
  (deadgrep-project-root-function 'projectile-project-root))

(use-package flycheck
  :diminish flycheck-mode
  :hook (prog-mode . global-flycheck-mode)
  :custom
  ;; (flycheck-check-syntax-automatically '(save idle-change new-line idle-buffer-switch mode-enabled))
  (flycheck-disabled-checkers '(emacs-lisp emacs-lisp-checkdoc javascript-jshint rustic-clippy))
  ;; (flycheck-javascript-eslint-executable "eslint_d")
  ;; (flycheck-help-echo-function nil)
  ;; (flycheck-display-errors-function nil)
  ;; (flycheck-global-modes '(not rust-ts-mode))
  :config
  (flycheck-add-mode 'javascript-eslint 'js-ts-mode)
  (flycheck-add-mode 'javascript-eslint 'typescript-ts-mode)
  (flycheck-add-mode 'javascript-eslint 'tsx-ts-mode)

  (add-to-list 'display-buffer-alist
             `(,(rx bos "*Flycheck errors*" eos)
              (display-buffer-reuse-window
               display-buffer-in-side-window)
              (side            . bottom)
              (reusable-frames . visible)
              (window-height   . 0.15))))

(use-package flycheck-posframe
  :ensure t
  :after flycheck
  :hook (flycheck-mode . flycheck-posframe-mode)
  :hook (flycheck-posframe-mode . flycheck-posframe-configure-pretty-defaults)
  :custom (flycheck-posframe-position 'window-bottom-right-corner))

(use-package yaml-mode
  :mode ("\\.ya?ml\\'" . yaml-mode))

(use-package markdown-mode
  :mode ("\\.md\\'" . markdown-mode))

(use-package highlight-indent-guides
  :hook (prog-mode . highlight-indent-guides-mode)
  :custom
  (highlight-indent-guides-auto-odd-face-perc 15)
  (highlight-indent-guides-auto-even-face-perc 7.5))

(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

(use-package rainbow-mode
  :hook (prog-mode . rainbow-mode)
  :diminish rainbow-mode)

(use-package whitespace
  :ensure nil
  :hook (prog-mode . whitespace-mode)
  :custom
  (whitespace-line-column 120)
  (whitespace-style '(face tabs empty trailing lines-tail tab-mark)))

(use-package ws-butler
  :hook ((text-mode prog-mode) . ws-butler-mode)
  :custom
  (ws-butler-keep-whitespace-before-point nil)
  (ws-butler-global-exempt-modes '(special-mode comint-mode term-mode eshell-mode diff-mode markdown-mode)))

(use-package hl-todo
  :defer 0
  :custom
  (hl-todo-highlight-punctuation ":")
  :config
  (global-hl-todo-mode))

(use-package goggles
  :commands goggles-mode
  :hook ((prog-mode text-mode) . goggles-mode)
  :custom
  (goggles-pulse t))

(use-package helpful
  :commands (helpful-function helpful-symbol helpful-variable helpful-command helpful-key)
  :bind
  ([remap describe-function] . helpful-function)
  ([remap describe-symbol] . helpful-symbol)
  ([remap describe-variable] . helpful-variable)
  ([remap describe-command] . helpful-command)
  ([remap describe-key] . helpful-key))

(use-package lua-mode
  :mode ("\\.lua\\'" . lua-mode)
  :hook (lua-mode . lsp-deferred)
  :hook (lua-mode . (lambda ()
                      (add-hook 'before-save-hook 'lsp-format-buffer nil t))))

(use-package csv-mode
  :disabled
  :mode ("\\.csv\\'" . csv-mode))

(use-package editorconfig
  :hook (prog-mode . editorconfig-mode))

(use-package dockerfile-mode
  :mode (("Dockerfile\\'" . dockerfile-mode)))

(use-package docker-compose-mode
  :mode (("docker-compose.yml\\'" . docker-compose-mode)
         ("docker-compose.yaml\\'" . docker-compose-mode)))

(use-package dotenv-mode
  :mode (("\\.env\\..*\\'" . dotenv-mode)
         ("\\.env\\'" . dotenv-mode)))

(use-package ibuffer
  :ensure nil
  :commands ibuffer
  :config
  (setq ibuffer-show-empty-filter-groups nil)

  (define-ibuffer-column size-h
    (:name "Size" :inline t)
    (cond
     ((> (buffer-size) 1000000) (format "%7.1fM" (/ (buffer-size) 1000000.0)))
     ((> (buffer-size) 100000) (format "%7.0fk" (/ (buffer-size) 1000.0)))
     ((> (buffer-size) 1000) (format "%7.1fk" (/ (buffer-size) 1000.0)))
     (t (format "%8d" (buffer-size)))))

  ;; Modify the default ibuffer-formats
  (setq ibuffer-formats
        '((mark modified read-only " "
                (name 18 18 :left :elide)
                " "
                (size-h 9 -1 :right)
                " "
                (mode 16 16 :left :elide)
                " "
                filename-and-process))))

(use-package ibuffer-projectile
  :after (ibuffer projectile)
  :commands ibuffer-projectile-set-filter-groups
  :hook (ibuffer . ibuffer-projectile-set-filter-groups))

(use-package apheleia
  :commands apheleia-mode
  :config
  (setf (alist-get 'prettier-json apheleia-formatters)
        '(npx "prettier" "--stdin-filepath" filepath))
  (setf (alist-get 'rustfmt apheleia-formatters)
        '("rustfmt" "+nightly" "--quiet" "--emit" "stdout")))

(use-package avy
  :bind (("C-c SPC" . avy-goto-char)
         ("C-c C-SPC" . avy-goto-char-timer))
  :custom
  (avy-background t)
  :config
  (push '(avy-goto-char-2 . avy-order-closest) avy-orders-alist)
  (push '(avy-goto-word-0 . avy-order-closest) avy-orders-alist)
  (push '(avy-goto-word-1 . avy-order-closest) avy-orders-alist)
  (push '(avy-goto-line . avy-order-closest) avy-orders-alist))

(use-package windmove
  :ensure nil
  :commands windmove-left windmove-right windmove-down windmove-up
  :bind (("S-<left>" . windmove-left)
         ("S-<right>" . windmove-right)
         ("S-<down>" . windmove-down)
         ("S-<up>" . windmove-up)))

(use-package timeout
  :ensure (timeout :type git :host github :repo "karthink/timeout")
  :commands timeout-throttle! timeout-debounce!
  :hook (on-first-input . setup-timeouts)
  :init
  (defun setup-timeouts ()
    (timeout-debounce! 'org-agenda-do-context-action 0.3)))

(use-package surround
  :ensure (surround :type git :host github :repo "mkleehammer/surround")
  :bind-keymap ("C-c s" . surround-keymap))

(use-package string-inflection
  :ensure (string-inflection :type git :host github :repo "akicho8/string-inflection")
  :commands string-inflection-all-cycle
  :bind ("C-c i" . string-inflection-all-cycle))

(provide 'ck-packages)
