(use-package treesit
  :ensure nil
  :custom
  (treesit-font-lock-level 4))

(use-package treesit-auto
  :ensure (treesit-auto
           :host github
           :repo "renzmann/treesit-auto"
           :files ("*.el"))
  :hook (on-first-input . global-treesit-auto-mode)
  :custom (treesit-auto-install 'prompt)
  :config
  (add-to-list 'treesit-language-source-alist `(typescript . ("https://github.com/tree-sitter/tree-sitter-typescript" nil "typescript/src" nil nil)))
  (add-to-list 'treesit-language-source-alist `(tsx . ("https://github.com/tree-sitter/tree-sitter-typescript" nil "tsx/src" nil nil)))
  (add-to-list 'treesit-language-source-alist `(elixir . ("https://github.com/elixir-lang/tree-sitter-elixir" nil nil nil nil)))
  (add-to-list 'treesit-language-source-alist `(heex-ts-mode . ("https://github.com/phoenixframework/tree-sitter-heex" nil nil nil nil)))
  (add-to-list 'treesit-language-source-alist `(ruby . ("https://github.com/tree-sitter/tree-sitter-ruby" nil nil nil nil)))
  (add-to-list 'treesit-language-source-alist `(scss . ("https://github.com/serenadeai/tree-sitter-scss" nil nil nil nil))))

(use-package combobulate
  :ensure (combobulate
           :host github
           :repo "mickeynp/combobulate"
           :files ("*.el"))
  :hook ((python-ts-mode . combobulate-mode)
         (js-ts-mode . combobulate-mode)
         (css-ts-mode . combobulate-mode)
         (yaml-ts-mode . combobulate-mode)
         (typescript-ts-mode . combobulate-mode)
         (tsx-ts-mode . combobulate-mode)
         ;; (combobulate-after-setup . (lambda ()
         ;;                              (when (eq major-mode 'tsx-ts-mode)
         ;;                                (local-unset-key "="))))
         ))

(provide 'ck-treesitter)
